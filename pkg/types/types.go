package types

import (
	"time"

	"github.com/jinzhu/gorm"
)

type User struct {
	Email       string `gorm:"primary_key"`
	Name        string
	Communities []Community `gorm:"many2many:user_communities"`
}

type Community struct {
	ID      string
	Name    string
	Users   []User `gorm:"many2many:user_communities" json:",omitempty"`
	Goal    int
	Current int
}

type Challenge struct {
	gorm.Model
	ActiveFrom  time.Time
	ActiveUntil time.Time
	Description string
	Goal        int
}

func (u User) IsIn(c string) bool {
	for _, cm := range u.Communities {
		if cm.ID == c {
			return true
		}
	}
	return false
}
