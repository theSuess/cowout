package server

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/pascaldekloe/jwt"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
)

func TestLogin(t *testing.T) {
	s := NewTestServer()
	rr := httptest.NewRecorder()
	payload := strings.NewReader(`{"email":"some.valid@email.com"}`)
	r := httptest.NewRequest("POST", "/auth/login", payload)
	r.Header.Set("content-type", "application/json")

	s.g.ServeHTTP(rr, r)

	b, _ := ioutil.ReadAll(rr.Result().Body)
	assert.Equal(t, http.StatusNoContent, rr.Result().StatusCode, "Response body: "+string(b))
	u := <-s.loginChan
	assert.Equal(t, "some.valid@email.com", u.Email)
}

func TestToken(t *testing.T) {
	viper.Set("jwtSecret", "testing")
	s := NewTestServer()

	loginClaim := new(jwt.Claims)
	loginClaim.Expires = jwt.NewNumericTime(time.Now().Add(time.Hour))
	loginClaim.Subject = "user.1@test.com"
	token, _ := loginClaim.HMACSign("HS512", []byte(viper.GetString("jwtSecret")))
	rr := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/auth/token/issue?token="+string(token), nil)

	s.g.ServeHTTP(rr, r)

	b, _ := ioutil.ReadAll(rr.Result().Body)
	assert.Equal(t, http.StatusOK, rr.Result().StatusCode, "Response body: "+string(b))
	tr := new(struct {
		Token string `json:"token"`
	})
	json.Unmarshal(b, tr)

	r = httptest.NewRequest("GET", "/auth/token/refresh", nil)
	r.Header.Set("X-Auth-Token", tr.Token)

	s.g.ServeHTTP(rr, r)
	b, _ = ioutil.ReadAll(rr.Result().Body)
	assert.Equal(t, http.StatusOK, rr.Result().StatusCode, "Response body: "+string(b))
}
