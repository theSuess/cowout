package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/smtp"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/pascaldekloe/jwt"
	"github.com/spf13/viper"
	templates "github.com/theSuess/cowout/pkg/server/mail"
	"github.com/theSuess/cowout/pkg/types"
	"github.com/theSuess/cowout/pkg/util/namegen"
	"go.uber.org/zap"
)

type UserClaim struct {
	jwt.Claims
	User types.User `json:"user"`
}

func (u UserClaim) Sign(dur time.Duration) (string, error) {
	u.Set = make(map[string]interface{})
	u.Set["user"] = u.User
	u.Subject = "user"
	u.Expires = jwt.NewNumericTime(time.Now().Add(dur))
	t, err := u.HMACSign("HS512", []byte(viper.GetString("jwtSecret")))
	if err != nil {
		return "", err
	}
	return string(t), nil
}

func (s *Server) AuthMiddleware(c *gin.Context) {
	t := c.GetHeader("X-Auth-Token")
	raw, err := jwt.HMACCheck([]byte(t), []byte(viper.GetString("jwtSecret")))
	if err != nil || time.Now().After(raw.Expires.Time()) || raw.Subject != "user" {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "unauthorized"})
		return
	}
	claim := UserClaim{}
	if err := json.Unmarshal(raw.Raw, &claim); err != nil {
		s.logger.Info("error extracting claims", zap.Error(err))
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "error during authentication"})
		return
	}
	c.Set("user", claim)
}

func (s *Server) RefreshToken(c *gin.Context) {
	ur, _ := c.Get("user")
	u := ur.(UserClaim)
	user := s.repo.GetUser(u.User.Email)
	if user == nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "could not find user for requesting token"})
	}
	u.User = *user
	t, err := u.Sign(time.Hour * 24 * 100)
	if err != nil {
		s.logger.Error("error signing claims for token refresh", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "internal server error"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"token": t})
}

func (s *Server) Login(c *gin.Context) {
	lr := new(struct {
		Email string `json:"email" binding:"email,required"`
	})
	err := c.ShouldBind(lr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	user := new(types.User)
	user.Email = lr.Email
	user.Name = namegen.NewName(3)
	s.repo.RegisterUser(*user)
	s.loginChan <- *user
	c.Status(http.StatusNoContent)
}

// IssueToken consumes a Token-Granting-Token (sent via email)
func (s *Server) IssueToken(c *gin.Context) {
	t := c.Query("token")
	loginclaim, err := jwt.HMACCheck([]byte(t), []byte(viper.GetString("jwtSecret")))
	if err != nil || time.Now().After(loginclaim.Expires.Time()) {
		c.JSON(http.StatusUnauthorized, gin.H{"message": "Invalid login token"})
		return
	}
	mail := loginclaim.Subject
	u := s.repo.GetUser(mail)
	if u == nil {
		c.JSON(http.StatusUnauthorized, gin.H{"message": "user not found"})
		return
	}
	uc := UserClaim{User: *u}
	t, err = uc.Sign(time.Hour * 24 * 100)
	if err != nil {
		s.logger.Error("error signing initial token", zap.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{"message": "internal server error"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"token": t})
}

func (s *Server) GetMe(c *gin.Context) {
	ur, _ := c.Get("user")
	u := ur.(UserClaim)
	dbu := s.repo.GetUser(u.User.Email)
	if dbu == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"message": "could not get current user"})
		return
	}
	c.JSON(http.StatusOK, dbu)
}

func (s *Server) SendLoginMails() {
	auth := smtp.PlainAuth("", viper.GetString("smtp.user"), viper.GetString("smtp.password"), viper.GetString("smtp.host"))
	for {
		user := <-s.loginChan
		if user.Name == "" {
			user.Name = "Newbie"
		}
		loginClaim := new(jwt.Claims)
		loginClaim.Expires = jwt.NewNumericTime(time.Now().Add(time.Hour))
		loginClaim.Subject = user.Email
		token, err := loginClaim.HMACSign("HS512", []byte(viper.GetString("jwtSecret")))
		if err != nil {
			s.logger.Error("could not sign login token", zap.Error(err))
			continue
		}

		loginURL := fmt.Sprintf(viper.GetString("webUrl")+"/#/login?token=%s", token)

		err = smtp.SendMail(viper.GetString("smtp.host")+":"+viper.GetString("smtp.port"), auth, viper.GetString("mail.from"), []string{user.Email}, []byte(templates.RenderText("cowout", loginURL)))
		if err != nil {
			s.logger.Error("could not send email", zap.Error(err))
			continue
		}
		s.logger.Info("sent mail", zap.String("email", user.Email))
	}
}
