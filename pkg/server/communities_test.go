package server

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"github.com/theSuess/cowout/pkg/types"
)

func TestGetCommunities(t *testing.T) {
	s := NewTestServer()
	rr := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/community/cm1", nil)

	s.g.ServeHTTP(rr, r)

	b, _ := ioutil.ReadAll(rr.Result().Body)
	assert.Equal(t, http.StatusOK, rr.Result().StatusCode, "Response body: "+string(b))
}

func TestCreateCommunity(t *testing.T) {
	viper.Set("jwtSecret", "testing")
	s := NewTestServer()
	u := s.repo.GetUser("user.1@test.com")
	uc := &UserClaim{
		User: *u,
	}
	token, _ := uc.Sign(time.Hour)
	rr := httptest.NewRecorder()
	r := httptest.NewRequest("POST", "/community/pkv", nil)
	r.Header.Add("X-Auth-Token", token)

	s.g.ServeHTTP(rr, r)

	b, _ := ioutil.ReadAll(rr.Result().Body)
	assert.Equal(t, http.StatusOK, rr.Result().StatusCode, "Response body: "+string(b))
	cm := s.repo.GetCommunity("pkv")
	assert.NotNil(t, cm, fmt.Sprintf("All Commununities: %+v", s.repo.GetAllCommunities()))
	assert.Equal(t, "pkv", cm.ID)
	assert.Equal(t, "user.1@test.com", cm.Users[0].Email)
}

func TestGetAllCommunities(t *testing.T) {
	s := NewTestServer()
	rr := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/community", nil)

	s.g.ServeHTTP(rr, r)

	b, _ := ioutil.ReadAll(rr.Result().Body)
	assert.Equal(t, http.StatusOK, rr.Result().StatusCode, "Response body: "+string(b))
}

func TestInvite(t *testing.T) {
	viper.Set("jwtSecret", "testing")
	s := NewTestServer()
	u := s.repo.GetUser("user.1@test.com")
	s.repo.JoinCommunity(*u, "cm1")
	uc := &UserClaim{
		User: *u,
	}
	token, _ := uc.Sign(time.Hour)

	rr := httptest.NewRecorder()
	r := httptest.NewRequest("POST", "/community/cm1/invite", nil)
	r.Header.Set("X-Auth-Token", string(token))

	s.g.ServeHTTP(rr, r)
	b, _ := ioutil.ReadAll(rr.Result().Body)
	assert.Equal(t, http.StatusCreated, rr.Result().StatusCode, "Response body: "+string(b))
	tr := struct {
		Token string `json:"token"`
	}{}
	json.Unmarshal(b, &tr)

	rr = httptest.NewRecorder()
	r = httptest.NewRequest("GET", "/join?token="+tr.Token, nil)
	r.Header.Set("X-Auth-Token", string(token))

	s.g.ServeHTTP(rr, r)

	b, _ = ioutil.ReadAll(rr.Result().Body)
	assert.Equal(t, http.StatusOK, rr.Result().StatusCode, "Response body: "+string(b))
}

func TestSubmit(t *testing.T) {
	viper.Set("jwtSecret", "testing")
	s := NewTestServer()
	u := s.repo.GetUser("user.1@test.com")
	s.repo.JoinCommunity(*u, "cm1")
	u.Communities = []types.Community{*s.repo.GetCommunity("cm1")}
	uc := &UserClaim{
		User: *u,
	}
	token, _ := uc.Sign(time.Hour)

	body := strings.NewReader(`{"amount":20}`)

	rr := httptest.NewRecorder()
	r := httptest.NewRequest("PUT", "/community/cm1", body)
	r.Header.Set("X-Auth-Token", string(token))
	r.Header.Set("content-type", "application/json")

	s.g.ServeHTTP(rr, r)

	b, _ := ioutil.ReadAll(rr.Result().Body)
	assert.Equal(t, http.StatusOK, rr.Result().StatusCode, "Response body: "+string(b))
	cm := s.repo.GetCommunity("cm1")
	assert.Equal(t, 20, cm.Current)
}
