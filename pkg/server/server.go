package server

import (
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	sendgrid "github.com/sendgrid/sendgrid-go"
	"github.com/spf13/viper"
	"github.com/theSuess/cowout/pkg/repository"
	"github.com/theSuess/cowout/pkg/types"
	"go.uber.org/zap"
)

type Server struct {
	g         *gin.Engine
	logger    *zap.Logger
	repo      repository.Repo
	sgclient  *sendgrid.Client
	loginChan chan (types.User)
}

func New(repo repository.Repo, logger *zap.Logger) *Server {
	repo.Init()
	s := &Server{
		logger: logger,
		repo:   repo,
	}
	s.Setup()
	return s
}

func (s *Server) Setup() {
	s.loginChan = make(chan types.User, 1024)

	g := gin.New()
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowHeaders = append(corsConfig.AllowHeaders, "X-Auth-Token")
	corsConfig.AllowOrigins = []string{"*"}
	g.Use(cors.New(corsConfig))
	auth := g.Group("/auth")
	auth.GET("/token/refresh", s.AuthMiddleware, s.RefreshToken)
	auth.GET("/me", s.AuthMiddleware, s.GetMe)
	auth.GET("/token/issue", s.IssueToken)
	auth.POST("/login", s.Login)
	g.GET("/join", s.AuthMiddleware, s.JoinCommunity)
	g.GET("/challenge", s.GetChallenge)
	g.GET("/community", s.GetCommunity)
	g.GET("/community/:id", s.GetCommunity)
	g.POST("/community/:id/invite", s.AuthMiddleware, s.CreateInvite)
	g.POST("/community/:id", s.AuthMiddleware, s.CreateCommunity)
	g.PUT("/community/:id", s.AuthMiddleware, s.SubmitWork)
	s.g = g
}

func (s *Server) Run() {
	go s.SendLoginMails()
	err := s.g.Run(viper.GetString("httpListenAddress"))
	s.logger.Error("error running the server", zap.Error(err))
}

func (s *Server) GetChallenge(c *gin.Context) {
	ch := s.repo.GetLatestChallenge()
	c.JSON(http.StatusOK, ch)
}
