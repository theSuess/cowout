package server

import (
	"errors"
	"strconv"

	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/theSuess/cowout/pkg/types"
	"go.uber.org/zap"
)

type MockRepository struct {
	users       map[string]*types.User
	communities map[string]*types.Community
}

func (r *MockRepository) Init() error {
	r.users = make(map[string]*types.User)
	r.communities = make(map[string]*types.Community)
	for i := 0; i < 10; i++ {
		r.users["user."+strconv.Itoa(i)+"@test.com"] = &types.User{
			Name:        "Test user " + strconv.Itoa(i),
			Email:       "user." + strconv.Itoa(i) + "@test.com",
			Communities: nil,
		}
	}

	for i := 0; i < 10; i++ {
		r.communities["cm"+strconv.Itoa(i)] = &types.Community{
			ID:   "cm" + strconv.Itoa(i),
			Name: "Test community " + strconv.Itoa(i),
		}
	}
	return nil
}
func (r *MockRepository) RegisterUser(u types.User) error {
	_, exists := r.users[u.Email]
	if exists {
		return errors.New("user aleready exists")
	}
	return nil
}

func (r *MockRepository) GetUser(email string) *types.User {
	return r.users[email]
}

func (r *MockRepository) GetCommunity(id string) *types.Community {
	return r.communities[id]
}

func (r *MockRepository) GetAllCommunities() []types.Community {
	var c []types.Community
	for _, v := range r.communities {
		c = append(c, *v)
	}
	return c
}

func (r *MockRepository) CreateCommunity(c types.Community) error {
	r.communities[c.ID] = &c
	return nil
}

func NewTestServer() *Server {
	logger, _ := zap.NewDevelopment()
	repo := &MockRepository{}

	gin.SetMode(gin.ReleaseMode)
	s := New(repo, logger)
	return s
}

func (r *MockRepository) JoinCommunity(u types.User, c string) error {
	u.Communities = append(u.Communities, *r.communities[c])
	r.users[u.Email] = &u
	r.communities[c].Users = append(r.communities[c].Users, u)
	return nil
}

func (r *MockRepository) SubmitWork(cid string, amount int) error {
	r.communities[cid].Current += amount
	return nil
}
