package server

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/pascaldekloe/jwt"
	"github.com/spf13/viper"
	"github.com/theSuess/cowout/pkg/types"
	"github.com/theSuess/cowout/pkg/util/namegen"
	"go.uber.org/zap"
)

func (s *Server) CreateCommunity(c *gin.Context) {
	cm := types.Community{}
	cm.ID = c.Param("id")
	uc, _ := c.Get("user")
	cm.Name = namegen.NewName(3)

	cm.Users = append(cm.Users, uc.(UserClaim).User)
	if err := s.repo.CreateCommunity(cm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "could not create community"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "created community, please refresh your token"})
}

func (s *Server) GetCommunity(c *gin.Context) {
	id := c.Param("id")
	if id != "" {
		cm := s.repo.GetCommunity(id)
		if cm == nil {
			c.JSON(http.StatusNotFound, gin.H{"message": "community not found"})
			return
		}
		c.JSON(http.StatusOK, cm)
		return
	}
	cm := s.repo.GetAllCommunities()
	c.JSON(http.StatusOK, cm)
}

func (s *Server) CreateInvite(c *gin.Context) {
	uc, _ := c.Get("user")
	cm := c.Param("id")
	u := s.repo.GetUser(uc.(UserClaim).User.Email)
	if !u.IsIn(cm) {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "user is not in community"})
		return
	}
	invite := jwt.Claims{}
	invite.Subject = cm
	invite.Expires = jwt.NewNumericTime(time.Now().Add(time.Hour))
	token, err := invite.HMACSign("HS512", []byte(viper.GetString("jwtSecret")))
	if err != nil {
		s.logger.Error("error signing invite", zap.Error(err))
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusCreated, gin.H{"token": string(token)})
	return
}

func (s *Server) JoinCommunity(c *gin.Context) {
	uc, _ := c.Get("user")
	u := uc.(UserClaim).User
	token := c.Query("token")
	claims, err := jwt.HMACCheck([]byte(token), []byte(viper.GetString("jwtSecret")))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "could not verify token"})
		return
	}
	if claims.Expires.Time().Before(time.Now()) {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "invite expired"})
		return
	}
	err = s.repo.JoinCommunity(u, claims.Subject)
	if err != nil {
		s.logger.Error("error joining community", zap.Error(err))
		c.AbortWithStatus(http.StatusInternalServerError)
	}
	c.Status(http.StatusOK)
}

func (s *Server) SubmitWork(c *gin.Context) {
	uc, _ := c.Get("user")
	u := uc.(UserClaim).User
	cid := c.Param("id")
	if !u.IsIn(cid) {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "you are not part of this community"})
		return
	}

	work := struct {
		Amount int `json:"amount" binding:"required"`
	}{}
	err := c.ShouldBind(&work)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	err = s.repo.SubmitWork(cid, work.Amount)
	if err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	c.Status(http.StatusOK)
}
