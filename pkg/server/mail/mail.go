package mail

import (
	html "html/template"
	"strings"
	"text/template"
)

const TextTemplate = `
Subject: Login request
Hey there!

It looks like you requested a login for {{.AppName}}.

Please click the link below.

{{.LoginURL}}

Happy Training!
`

const HTMLTemplate = `
Hey there! </br>

It looks like you requested a login for <strong>{{.AppName}}</strong>. </br>

Please click <a href="{{.LoginURL}}">here</a> to authenticate. </br>

Happy Training!
`

func RenderHTML(app, url string) string {
	t := html.Must(html.New("html").Parse(HTMLTemplate))
	builder := &strings.Builder{}
	t.Execute(builder, struct {
		AppName  string
		LoginURL string
	}{AppName: app, LoginURL: url})
	return builder.String()
}

func RenderText(app, url string) string {
	t := template.Must(template.New("plain").Parse(TextTemplate))
	builder := &strings.Builder{}
	t.Execute(builder, struct {
		AppName  string
		LoginURL string
	}{AppName: app, LoginURL: url})
	return builder.String()
}
