package repository

import "github.com/theSuess/cowout/pkg/types"

type Repo interface {
	Init() error
	RegisterUser(u types.User) error
	GetUser(email string) *types.User
	GetCommunity(id string) *types.Community
	GetAllCommunities() []types.Community
	CreateCommunity(c types.Community) error
	JoinCommunity(u types.User, c string) error
	SubmitWork(cid string, amount int) error
	GetLatestChallenge() *types.Challenge
}
