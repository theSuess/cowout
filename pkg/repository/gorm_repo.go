package repository

import (
	"time"

	"github.com/jinzhu/gorm"
	"github.com/theSuess/cowout/pkg/types"
)

type GormRepository struct {
	db *gorm.DB
}

func NewGorm(db *gorm.DB) *GormRepository {
	return &GormRepository{
		db: db,
	}
}

func (r *GormRepository) Init() error {
	return r.db.AutoMigrate(&types.User{}, &types.Community{}, &types.Challenge{}).Error
}

func (r *GormRepository) RegisterUser(u types.User) error {
	return r.db.Create(&u).Error
}

func (r *GormRepository) GetUser(email string) *types.User {
	u := new(types.User)
	r.db.Preload("Communities").First(u, "email = ?", email)
	if u.Email == "" {
		return nil
	}
	return u
}

func (r *GormRepository) GetCommunity(id string) *types.Community {
	c := new(types.Community)
	r.db.First(c, "id = ?", id)
	if c.ID == "" {
		return nil
	}
	return c
}

func (r *GormRepository) GetAllCommunities() []types.Community {
	var c []types.Community
	r.db.Find(&c)
	return c
}

func (r *GormRepository) CreateCommunity(c types.Community) error {
	return r.db.Create(c).Error
}

func (r *GormRepository) JoinCommunity(u types.User, c string) error {
	return r.db.Model(&u).Association("Communities").Append(&types.Community{ID: c}).Error
}

func (r *GormRepository) SubmitWork(cid string, amount int) error {
	tx := r.db.Begin()
	c := new(types.Community)
	err := tx.First(c, "id = ?", cid).Error
	if err != nil {
		return err
	}
	c.Current += amount
	tx.Update(c)
	return tx.Commit().Error
}

func (r *GormRepository) GetLatestChallenge() *types.Challenge {
	c := new(types.Challenge)
	err := r.db.First(c, "active_from < ? AND active_until > ?", time.Now(), time.Now()).Error
	if err != nil {
		return nil
	}
	return c
}
