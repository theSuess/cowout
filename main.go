package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/spf13/viper"
	"github.com/theSuess/cowout/pkg/repository"
	"github.com/theSuess/cowout/pkg/server"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func main() {
	config := zap.NewDevelopmentConfig()
	config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	logger, _ := config.Build()

	viper.SetConfigName("cowout")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		logger.Fatal("error reading the configuration", zap.Error(err))
	}

	db, err := gorm.Open("sqlite3", "main.db")
	if err != nil {
		logger.Fatal("error connecting to the database", zap.Error(err))
	}
	//db.SetLogger(gormzap.New(logger))
	db.LogMode(true)

	repo := repository.NewGorm(db)

	s := server.New(repo, logger)
	s.Run()
}
