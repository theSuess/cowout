module github.com/theSuess/cowout

require (
	cloud.google.com/go v0.35.1
	github.com/gin-contrib/cors v0.0.0-20190101123304-5e7acb10687f
	github.com/gin-contrib/sse v0.0.0-20190125020943-a7658810eb74 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/jinzhu/gorm v1.9.2
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/mailgun/mailgun-go/v3 v3.3.0
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/nicklaw5/go-respond v0.0.0-20181019234323-81e2b0dd5bb6
	github.com/pascaldekloe/jwt v1.2.1
	github.com/sendgrid/rest v2.4.1+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.4.1+incompatible
	github.com/speps/go-hashids v2.0.0+incompatible
	github.com/spf13/viper v1.3.1
	github.com/stretchr/testify v1.2.2
	github.com/wantedly/gorm-zap v0.0.0-20171015071652-372d3517a876 // indirect
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.9.1
)
